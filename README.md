![Flathub](https://img.shields.io/flathub/v/dev.zelikos.rollit?style=for-the-badge)
![Installs](https://img.shields.io/flathub/downloads/dev.zelikos.rollit?style=for-the-badge)

![Icon](data/icons/hicolor/scalable/apps/dev.zelikos.rollit.svg)

# Chance

Roll six-sided dice by default, or roll custom dice of up to 999 sides.

![Screenshot](data/screenshots/01_rollit_wide_1.png)

## Installation

The latest stable release of Chance is available via Flathub.

<a href='https://flathub.org/apps/details/dev.zelikos.rollit'><img width='240' alt='Download on Flathub' src='https://dl.flathub.org/assets/badges/flathub-badge-en.png'/></a>

Any version distributed elsewhere is not provided nor supported by me.

## Building

Chance is built using GTK4 and libadwaita with the [GNOME HIG](https://developer.gnome.org/hig/) in mind.

If you would like to build and hack on Chance, it is highly recommended to use [GNOME Builder](https://flathub.org/apps/org.gnome.Builder).

Manual build instructions to come later.

## Credits

Other GTK Rust projects used for reference:

- [GTK Rust Template](https://gitlab.gnome.org/World/Rust/gtk-rust-template)
- [Loupe](https://gitlab.gnome.org/Incubator/loupe)
- [PikaBackup](https://gitlab.gnome.org/World/pika-backup)
- [Amberol](https://gitlab.gnome.org/World/amberol)

